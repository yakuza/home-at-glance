﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[ExecuteInEditMode]
public class SevenSegmentsController : MonoBehaviour
{
    [Flags]
    private enum Segment
    {
        Top = 1,
        TopLeft = 2,
        TopRight = 4,
        Middle = 8,
        Bottom = 16,
        BottomLeft = 32,
        BottomRight = 64
    }

    [SerializeField] private Material _offMaterial;
    [SerializeField] private Material _onMaterial;
    [SerializeField] private int _digit;

    private readonly Dictionary<Segment, GameObject> _segments = new Dictionary<Segment, GameObject> ();

    private readonly Dictionary<int, Segment[]> _digitToSegments = new Dictionary<int, Segment[]> {
        { -1, new Segment[] { Segment.Top, Segment.Middle, Segment.Bottom } },
        { 0, new Segment[] { Segment.Top, Segment.TopRight, Segment.BottomRight, Segment.Bottom, Segment.BottomLeft, Segment.TopLeft } },
        { 1, new Segment[] { Segment.TopRight, Segment.BottomRight } },
        { 2, new Segment[] { Segment.Top, Segment.TopRight, Segment.Middle, Segment.BottomLeft, Segment.Bottom } },
        { 3, new Segment[] { Segment.Top, Segment.TopRight, Segment.Middle, Segment.BottomRight, Segment.Bottom } },
        { 4, new Segment[] { Segment.TopLeft, Segment.TopRight, Segment.Middle, Segment.BottomRight } },
        { 5, new Segment[] { Segment.Top, Segment.TopLeft, Segment.Middle, Segment.BottomRight, Segment.Bottom } },
        { 6, new Segment[] { Segment.Top, Segment.TopLeft, Segment.Middle, Segment.BottomRight, Segment.Bottom, Segment.BottomLeft } },
        { 7, new Segment[] { Segment.Top, Segment.TopRight, Segment.BottomRight } },
        { 8, new Segment[] { Segment.Top, Segment.TopRight, Segment.TopLeft, Segment.Middle, Segment.BottomRight, Segment.Bottom, Segment.BottomLeft } },
        { 9, new Segment[] { Segment.Top, Segment.TopRight, Segment.TopLeft, Segment.Middle, Segment.BottomRight, Segment.Bottom } }
    };

    private void Start()
    {
        _segments.Clear ();
        foreach (Transform child in transform.GetChild(0))
        {
            if (child.name.StartsWith ("Segment_") == false)
                continue;

            var segmentName = child.name.Substring (8);
            var segment = (Segment)Enum.Parse (typeof(Segment), segmentName);

            _segments [segment] = child.gameObject;
        }

        SetDigit (_digit);
    }

    public void SetDigit(int digit) {
        if (_segments.Count == 0)
        {
            return;
        }

        if (digit < 0 || digit > 9)
        {
            digit = -1;
        }

        foreach (var segmentObject in _segments.Values)
        {
            segmentObject.GetComponent<MeshRenderer> ().material = _offMaterial;
        }

        foreach (var segment in _digitToSegments[digit])
        {
            _segments [segment].GetComponent<MeshRenderer> ().material = _onMaterial;
        }
        _digit = digit;
    }

    private void OnValidate() {
        SetDigit (_digit);
    }
}
