﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColonSegmentController : MonoBehaviour {
    private readonly List<MeshRenderer> _segments = new List<MeshRenderer> ();

    [SerializeField] private Material _onMaterial;
    [SerializeField] private Material _offMaterial;

	private void Start () {
        foreach (Transform child in transform)
        {
            if (child.name.StartsWith ("Segment_"))
            {
                _segments.Add (child.GetComponent<MeshRenderer> ());
            }
        }
	}

    public void TurnOn() {
        foreach (var segment in _segments)
        {
            segment.material = _onMaterial;
        }
    }

    public void TurnOff() {
        foreach (var segment in _segments)
        {
            segment.material = _offMaterial;
        }
    }
}
