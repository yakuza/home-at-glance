﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;

public class SegmentCalendarController : MonoBehaviour {
    [SerializeField] private SevenSegmentsController _year1;
    [SerializeField] private SevenSegmentsController _year2;
    [SerializeField] private SevenSegmentsController _year3;
    [SerializeField] private SevenSegmentsController _year4;

    [SerializeField] private SevenSegmentsController _month1;
    [SerializeField] private SevenSegmentsController _month2;

    [SerializeField] private SevenSegmentsController _day1;
    [SerializeField] private SevenSegmentsController _day2;

    private IDisposable _timer;

	private void Start () {
        _timer = Observable.Interval (TimeSpan.FromSeconds (1)).Subscribe (UpdateDate);
	}

    private void UpdateDate(long ticks) {
        var today = DateTime.Now;

        var year = today.Year;
        var year1Digit = year / 1000;
        var year2Digit = (year % 1000) / 100;
        var year3Digit = (year % 100) / 10;
        var year4Digit = year % 10;

        var month = today.Month;
        var month1Digit = month / 10;
        var month2Digit = month % 10;

        var day = today.Day;
        var day1Digit = day / 10;
        var day2Digit = day % 10;

        _year1.SetDigit (year1Digit);
        _year2.SetDigit (year2Digit);
        _year3.SetDigit (year3Digit);
        _year4.SetDigit (year4Digit);

        _month1.SetDigit (month1Digit);
        _month2.SetDigit (month2Digit);

        _day1.SetDigit (day1Digit);
        _day2.SetDigit (day2Digit);
    }

    private void OnDestroy() {
        _timer.Dispose ();
    }
}
