﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;
using UniRx;

public class SegmentClockController : MonoBehaviour {
    [Inject(Id="hours-1")] private SevenSegmentsController _hoursTensController;
    [Inject(Id="hours-2")] private SevenSegmentsController _hoursOnesController;
    [Inject(Id="minutes-1")] private SevenSegmentsController _minutesTensController;
    [Inject(Id="minutes-2")] private SevenSegmentsController _minutesOnesController;
    [Inject] private ColonSegmentController _colonController;

    private IDisposable _timer;

	private void Start () {
        _timer = Observable.Interval (TimeSpan.FromSeconds (1)).Subscribe (UpdateClock);
	}

    private void UpdateClock (long ticks) {
        var now = DateTime.Now;

        var hoursTens = now.Hour / 10;
        var hoursOnes = now.Hour % 10;
        var minutesTens = now.Minute / 10;
        var minutesOnes = now.Minute % 10;

        _hoursTensController.SetDigit (hoursTens);
        _hoursOnesController.SetDigit (hoursOnes);
        _minutesTensController.SetDigit (minutesTens);
        _minutesOnesController.SetDigit (minutesOnes);

        if (ticks % 2 == 0)
        {
            _colonController.TurnOff ();
        }
        else
        {
            _colonController.TurnOn ();
        }
    }
	
    private void OnDestroy () {
        _timer.Dispose ();
    }
}
