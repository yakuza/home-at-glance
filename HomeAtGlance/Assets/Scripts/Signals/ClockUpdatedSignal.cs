﻿using System;
using Zenject;

namespace AssemblyCSharp
{
    public class ClockUpdatedSignal : Signal<ClockUpdatedSignal, long>
    {
        public ClockUpdatedSignal()
        {
        }
    }
}

